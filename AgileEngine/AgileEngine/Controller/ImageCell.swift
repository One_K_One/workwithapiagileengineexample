//
//  ImageCell.swift
//  AgileEngine
//
//  Created by Константин Овчаренко on 3/20/19..
//  Copyright © 2019 Константин Овчаренко. All rights reserved.
//

import UIKit

class PhotoCell : UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
}
