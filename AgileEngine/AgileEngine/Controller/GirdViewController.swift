//
//  ViewController.swift
//  AgileEngine
//
//  Created by Константин Овчаренко on 3/20/19..
//  Copyright © 2019 Константин Овчаренко. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class GirdViewController: UICollectionViewController {    

    fileprivate let reuseIdentifier = "Cell"
    fileprivate let thumbnailSize = CGSize(width:170.0, height: 170.0)
    fileprivate let sectionInsets = UIEdgeInsets(top: 10, left: 5.0, bottom: 10.0, right: 5.0)
    fileprivate var photos : [ImageModel] = []
    
    typealias complitionHablerImages         = (_ images     : UIImageView) -> ()
    
  
    let apiManage = ApiManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.apiManage.auth(complition: {(success: Bool ) in
            if success{
                self.apiManage.getImages(page: 1, complition: { (photos:[ImageModel]) in
                    self.photos.append(contentsOf: photos)
                    self.collectionView.reloadData()
                })
            } else {
                print("покажи тут алерт вместо этого")
            }
        })
        
        
        
//        ApiManager.shared.auth {
//            ApiManager.shared.getImages(page:complitionHablerPageNumber, complition: { (ImageModels : [ImageModel]) in
//                <#code#>
//            })
//        }
    }
    
}

// MARK: UICollectionViewDataSource

extension GirdViewController {
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PhotoCell
        let imageModel = self.photos[indexPath.row]
        Alamofire.request(imageModel.previewURL).responseImage { response in
            guard let image = UIImage(data:response.data!) else {
                // Handle error
                return
            }
            let imageData = UIImage.jpegData(image)(compressionQuality: 1.0)
            cell.imageView.image = UIImage(data : imageData!)
        }
       // cell.imageView.image = fullSizedImage?.thumbnailOfSize(thumbnailSize)
        return cell
    }
}

// MARK:UICollectionViewDelegateFlowLayout
extension GirdViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return thumbnailSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
}
