//
//  UIImage.swift
//  AgileEngine
//
//  Created by Константин Овчаренко on 3/20/19..
//  Copyright © 2019 Константин Овчаренко. All rights reserved.
//

import UIKit

extension UIImage {
    func thumbnailOfSize(_ newSize: CGSize) -> UIImage? {
        let renderer = UIGraphicsImageRenderer(size: newSize)
        let thumbnail = renderer.image {_ in
            self.draw(in: CGRect.init(origin: CGPoint.zero, size: newSize))
        }
        return thumbnail
    }
}
