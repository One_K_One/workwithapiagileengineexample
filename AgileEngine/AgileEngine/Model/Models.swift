//
//  Models.swift
//  AgileEngine
//
//  Created by Константин Овчаренко on 3/24/19.
//  Copyright © 2019 Константин Овчаренко. All rights reserved.
//

import Foundation



class ImageModel : Codable {
    let id : String
    let previewURL : String
    var detail : ImageDetailModel?
    
    
    enum CodingKeys: String, CodingKey {
        case id
        case previewURL = "cropped_picture"
        case detail
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        previewURL = try container.decode(String.self, forKey: .previewURL)
        detail = try? container.decode(ImageDetailModel.self, forKey: .detail)
    }
}

class ImageDetailModel : Codable {
    let author : String
    let fullPhotoURL : String
    
    enum CodingKeys: String, CodingKey {
        case author
        case fullPhotoURL
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        author = try container.decode(String.self, forKey: .author)
        fullPhotoURL = try container.decode(String.self, forKey: .fullPhotoURL)
    }
}

class ImagesPageModel : Codable {
    var imageModels : [ImageModel] = []
    var imageCurrenPage : Int
    var imagesCountOfPage : Int
    
    enum CodingKeys: String, CodingKey {
        case imageModels = "pictures"
        case imageCurrenPage = "page"
        case imagesCountOfPage = "pageCount"
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        imageModels = try container.decode([ImageModel].self, forKey: .imageModels)
        imageCurrenPage = try container.decode(Int.self, forKey: .imageCurrenPage)
        imagesCountOfPage = try container.decode(Int.self, forKey: .imagesCountOfPage)
    }
}

class TokenModel : Decodable {
    let token : String?
    
    enum CodingKeys: String, CodingKey {
        case token = "token"
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        token = try? container.decode(String.self, forKey: .token)
    }
}
