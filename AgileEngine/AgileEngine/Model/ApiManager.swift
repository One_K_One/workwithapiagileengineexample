//
//  ApiManager.swift
//  AgileEngine
//
//  Created by Константин Овчаренко on 3/24/19.
//  Copyright © 2019 Константин Овчаренко. All rights reserved.
//

import Foundation


class ApiManager  {
    
    
   private init(){
        
    }
    
    static let shared = ApiManager()
    private var token : String?
    private var arrayOfImageURL = [""]
    

    func auth(complition: @escaping (Bool) -> ()){
        let request = Request()
        request.getRequest { (token : String?) -> () in
            self.token = token
            OperationQueue.main.addOperation {
                 complition(token != nil)
            }
        }
        
    }
    
    func getImages(page : Int , complition : @escaping ([ImageModel]) -> ()) {
        
        guard let token = self.token else {
            OperationQueue.main.addOperation {
                complition([])
            }
            return
        }

        let request = Request()
        request.postRequestDetail(token: token, pageNumber: page) { (imageModels :[ImageModel]) in
            OperationQueue.main.addOperation {
                complition(imageModels)
            }
        }
    }
}
